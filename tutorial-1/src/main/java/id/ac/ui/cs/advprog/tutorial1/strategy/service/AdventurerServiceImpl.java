package id.ac.ui.cs.advprog.tutorial1.strategy.service;

import id.ac.ui.cs.advprog.tutorial1.strategy.core.*;
import id.ac.ui.cs.advprog.tutorial1.strategy.repository.AdventurerRepository;
import id.ac.ui.cs.advprog.tutorial1.strategy.repository.StrategyRepository;
import org.springframework.stereotype.Service;

@Service
public class AdventurerServiceImpl implements AdventurerService {

    private final AdventurerRepository adventurerRepository;

    private final StrategyRepository strategyRepository;

    public AdventurerServiceImpl(
            AdventurerRepository adventurerRepository,
            StrategyRepository strategyRepository) {

        this.adventurerRepository = adventurerRepository;
        this.strategyRepository = strategyRepository;
    }

    @Override
    public Iterable<Adventurer> findAll() {
        return adventurerRepository.findAll();
    }

    @Override
    public Adventurer findByAlias(String alias) {
        return adventurerRepository.findByAlias(alias);
    }

    @Override
    public void changeStrategy(String alias, String attackType, String defenseType) {
        Adventurer objectToChange = findByAlias(alias);
        if (attackType.equalsIgnoreCase("gun") || attackType.equalsIgnoreCase("magic") || attackType.equalsIgnoreCase("sword")) {
            objectToChange.setAttackBehavior(strategyRepository.getAttackBehaviorByType(attackType));
        }

        if (defenseType.equalsIgnoreCase("armor") || defenseType.equalsIgnoreCase("barrier") || defenseType.equalsIgnoreCase("shield")) {
            objectToChange.setDefenseBehavior(strategyRepository.getDefenseBehaviorByType(defenseType));
        }
    }

    @Override
    public Iterable<AttackBehavior> getAttackBehaviors() {
        return strategyRepository.getAttackBehaviors();
    }

    @Override
    public Iterable<DefenseBehavior> getDefenseBehaviors() {
        return strategyRepository.getDefenseBehaviors();
    }
}
