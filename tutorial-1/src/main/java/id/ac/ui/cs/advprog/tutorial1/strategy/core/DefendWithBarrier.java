package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithBarrier implements DefenseBehavior {
    @Override
    public String getType() {
        return "Barrier";
    }

    @Override
    public String defend() {
        return "Defend With Barrier, hahaha is that your best attack ?";
    }
}
